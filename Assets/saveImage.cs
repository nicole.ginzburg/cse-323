﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class saveImage : MonoBehaviour
{
    int numPicsSaved = 0;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(save);
        //save();
    }

    // Update is called once per frame
    void Update()
    { }

    void save() {
        ScreenCapture.CaptureScreenshot("image_" + numPicsSaved + ".png"); //append a number to prevent the previous picture from being overwritten
       // DrawMain.drawingCanvas.gameObject.layer = 8;
        numPicsSaved++;
        Debug.Log("Picture saved! " + numPicsSaved);
    }
    

}
