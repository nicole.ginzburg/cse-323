﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class changetemplate : MonoBehaviour {
	public Sprite[] templateList;
//	List<Sprite> templateList2 = new List<Sprite>();
//	public Sprite spriteA;
//	public Sprite spriteB;
	int index = 0;

	void Start () {

		GetComponent<SpriteRenderer>().sprite = templateList[index];

	}
		
	void Update () {

		//change template by pressing L
		if (Input.GetKeyDown(KeyCode.L))
			{
			if (index == templateList.Length - 1) {
				index = 0;
				GetComponent<SpriteRenderer> ().sprite = templateList[index];
			} else {
				GetComponent<SpriteRenderer> ().sprite = templateList[++index];
			}
			}

		//change template by pressing J
		if (Input.GetKeyDown(KeyCode.J))
		{
			if (index == 0) {
				index = templateList.Length - 1;
				GetComponent<SpriteRenderer>().sprite = templateList[index];
			} else {
				GetComponent<SpriteRenderer>().sprite = templateList[--index];
			}

		}

		//make template at 50% opacity
		GetComponent<SpriteRenderer>().color = new Color (1f, 1f, 1f, 0.5f);

		//if (Input.GetKeyUp (KeyCode.P))
		//		{
			//		GetComponent<SpriteRenderer>().sprite = spriteB;
			//	}
	}
}


