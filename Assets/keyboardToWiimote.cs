﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WiimoteApi;

public class keyboardToWiimote : MonoBehaviour
{
    public Wiimote wm; //wiimote stuff happens here.
    public GameObject wiimoteCursorObject; //an object to act as a "cursor/mouse pointer" so draw functions can get coordinate data

    private bool wiimoteConnected = false;

    void Start()
    {
        Debug.Log("keyboardToWiimote: Initiallized!");
    }
    void handleKeypress()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (wiimoteConnected)
        {
            Debug.Log("kwm/handleKeypress: Looks like a wiimote has been connected. Here's a list of joystick devices: " + Input.GetJoystickNames());
            handleWiimoteEvent();
            return;
        }
        if (Input.GetMouseButton(0))
            SendMessageUpwards("beginDraw");
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("keyboardToWiimote/handleKeypress: We pressed the 'W' key!");
            DrawMain.setWidth(1);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("keyboardToWiimote/handleKeypress: We pressed the 'A' key!");
            DrawMain.cycleColor(-1);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            Debug.Log("keyboardToWiimote/handleKeypress: We pressed the 'S' key!");
            DrawMain.setWidth(-1);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("keyboardToWiimote/handleKeypress: We pressed the 'D' key!");
            DrawMain.cycleColor(1);
        }
        else if (Input.GetKey(KeyCode.O))
        {
            DrawMain.changeWallBackground();
        }

    }
    void handleWiimoteEvent()
    {
        if (wm.ClassicController.b || wm.Button.b)
        {
            //what if an invisible gameObject followed the wiimote when it moved (to get screen coordiantes) and we passed those coords to drawable?
            //no math involved, plus if unity sees the wiimote as a joystick then it should update the Input axes 
            //(so we don't need to deal with the raw accel data array)
            DrawMain.wiimoteDraw(wiimoteCursorObject.transform.position);
        }

        if (wm.ClassicController.dpad_up || wm.Button.d_up)
        {
            Debug.Log("keyboardToWiimote/handleWiimoteEvent: Pressed the D-pad (up) ");
            DrawMain.changeWallBackground();
        }
        else if (wm.ClassicController.dpad_down || wm.Button.d_down)
        {
            Debug.Log("keyboardToWiimote/handleWiimoteEvent: Pressed the D-pad (down)");
            DrawMain.changeWallBackground();
        }
        else if (wm.ClassicController.dpad_right || wm.Button.d_right)
        {
            Debug.Log("keyboardToWiimote/handleWiimoteEvent: Pressed the D-pad (right), the current color is " + DrawMain.getCurrentColor());
            DrawMain.cycleColor(1);
        }
        else if (wm.ClassicController.dpad_left || wm.Button.d_left)
        {
            Debug.Log("keyboardToWiimote/handleWiimoteEvent: Pressed the D-pad (left), the current color is " + DrawMain.getCurrentColor());
            DrawMain.cycleColor(-1);
        }
        else if (wm.ClassicController.plus || wm.Button.plus)
        {
            Debug.Log("keyboardToWiimote/handleWiimoteEvent: Pressed plus button");
            DrawMain.setWidth(1);
        }

        else if (wm.ClassicController.minus || wm.Button.minus)
        {
            Debug.Log("keyboardToWiimote/handleWiimoteEvent: Pressed minus button");
            DrawMain.setWidth(-1);
        }
        //Debug.Log(wm.Accel.GetCalibratedAccelData());
    }

    // Update is called once per frame
    void Update()
    { 
        if (WiimoteManager.FindWiimotes() && !wiimoteConnected)
        {
            Debug.Log("kwm: wiimote connected!");
            wm = WiimoteManager.Wiimotes.ToArray()[0]; //get the first wiimote
            wiimoteConnected = true;
        }
        if (wiimoteConnected)
            wiimoteCursorObject.transform.Translate(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")));
        else if (WiimoteManager.Wiimotes.Count == 0)
            wiimoteConnected = false;
    }

     void OnApplicationQuit()
    {
        if (wm != null)
        {
            WiimoteManager.Cleanup(wm);
            wm = null;
        }
    }
}

